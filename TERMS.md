# Terms and Conditions

The Air Quality app is provided as-is. You may only use the service as designed. An Alexa-enabled device 
must be used to access the service. Attempts to reverse engineer the app or access the voice API are 
expressly forbidden.

Please report bugs or vulnerabilities to brandon.telle@gmail.com