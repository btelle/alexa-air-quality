# Privacy Policy

The Air Quality app was built as a free app. This SERVICE is provided at no cost and is intended for use as is.

This page is used to inform visitors regarding our policies with the collection, use, and disclosure of Personal Information if anyone decided to use our Service.

If you choose to use the Service, then you agree to the collection and use of information in relation to this policy. The Personal Information that we collect is used for providing and improving the Service. We will not use or share your information with anyone except as described in this Privacy Policy.

The terms used in this Privacy Policy have the same meanings as in our Terms and Conditions, which is accessible at Air Quality unless otherwise defined in this Privacy Policy.

## Information Collection and Use

The Service requests to use Device Location data to enhance the service offered. Your locations are not stored, logged, or otherwise saved by the The Service. You may use the The Service without providing permission to use your Device Location. You may opt out of the Device Location sharing at any time.

## Log Data

The Service logs information about each invocation of the service for debugging and stability reasons. Personally identifiable information is not included in these logs. Logs are retained for 90 days then are permanently deleted. 

We will not share log data with anyone unless compelled to by court order. 
