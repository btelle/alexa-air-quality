# Alexa Air Quality

Alexa app to check the AQICN API for the current air quality index reading for a given location.

## Building

Install dependencies to local dir:

```
pip install -r requirements.txt -t ./
```

Build zip archive:

```
zip -r alexa-air-quality.zip ./
```

Upload to Lambda function:

```
aws lambda update-function-code --function-name <ARN> --zip-file fileb://alexa-air-quality.zip
```
