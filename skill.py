import requests
from ask import alexa
from config import AQICN_API_KEY

class ApiException(Exception):
    pass

RESPONSES = {
    "welcome": "Air quality checks the Air Quality Index for your current location, or any city. Ask how is the air outside, or how is the air in Seattle.",
    "cancel": "Goodbye.",
    "help": "Ask me how the air quality is outside, or in a city.",
    "fallback": "I cannot help with that, but I can check the Air Quality Index for you. Ask me how the air quality is outside, or how is the air quality in Seattle.",
    "request_permissions": "Please enable location permissions to get local air quality.",
    "not_found": "I could not locate a station for that location. Please try again.",
    "air_quality": "The air quality is {}."
}

def lambda_handler(request_obj, context=None):
    '''
    This is the main function to enter to enter into this code.
    If you are hosting this code on AWS Lambda, this should be the entry point.
    Otherwise your server can hit this code as long as you remember that the
    input 'request_obj' is JSON request converted into a nested python object.
    '''

    metadata = {'user_name' : 'Brandon Telle'}
    print request_obj

    ''' inject user relevant metadata into the request if you want to, here.
    e.g. Something like :
    ... metadata = {'user_name' : some_database.query_user_name(request.get_user_id())}

    Then in the handler function you can do something like -
    ... return alexa.create_response('Hello there {}!'.format(request.metadata['user_name']))
    '''
    return alexa.route_request(request_obj, metadata)

@alexa.default_handler()
def default_handler(request):
    return alexa.create_response(message=RESPONSES['welcome'],
    		reprompt_message=RESPONSES['welcome'])

@alexa.intent_handler("AMAZON.HelpIntent")
def help_handler(request):
    return alexa.create_response(message=RESPONSES['help'],
    		reprompt_message=RESPONSES['help'])

@alexa.intent_handler("AMAZON.FallbackIntent")
def cancel_handler(request):
	return alexa.create_response(message=RESPONSES['fallback'],
            reprompt_message=RESPONSES['fallback'])

@alexa.intent_handler("AMAZON.StopIntent")
def stop_handler(request):
	return alexa.create_response(message=RESPONSES['cancel'], end_session=True)

@alexa.intent_handler("AMAZON.CancelIntent")
def cancel_handler(request):
	return alexa.create_response(message=RESPONSES['cancel'], end_session=True)

@alexa.intent_handler("AirQualityDeviceLocation")
def air_quality_local(request):
    try:
        access_token = request.request.get('context').get('System').get('apiAccessToken')
        api_endpoint = request.request.get('context').get('System').get('apiEndpoint')
        device_id = request.request.get('context').get('System').get('device').get('deviceId')
        permissions = request.request.get('context').get('System').get('user', {}).get('permissions', False)

        try:
            if permissions:
                zip_code = get_zip(access_token, device_id, api_endpoint)
            else:
                return alexa.create_response(
                    message=RESPONSES['request_permissions'],
                    end_session=True,
                    card_obj={
                        "type": "AskForPermissionsConsent",
                        "permissions": [
                            "read::alexa:device:all:address:country_and_postal_code"
                        ]
                    }
                )

            (lat,lon) = zip_to_lat_long(zip_code)
            aqi = get_air_quality_index(lat, lon)
            return alexa.create_response(message=get_response_by_aqi(aqi), end_session=True)
        except ApiException:
            # dynamic permission display
            return alexa.create_response(
                message=RESPONSES['request_permissions'],
                end_session=True,
                card_obj={
                    "type": "AskForPermissionsConsent",
                    "permissions": [
                        "read::alexa:device:all:address:country_and_postal_code"
                    ]
                }
            )
    except:
        return alexa.create_response(message=RESPONSES['not_found'], end_session=True)

@alexa.intent_handler("AirQualityCity")
def air_quality_city(request):
    try:
        city = request.slots.get('City')
        (lat,lon) = city_to_lat_long(city)

        aqi = get_air_quality_index(lat, lon)
        return alexa.create_response(message=get_response_by_aqi(aqi, city), end_session=True)

    except:
        return alexa.create_response(message=RESPONSES['not_found'], end_session=True)

@alexa.intent_handler("AirQualityCityState")
def air_quality_city_state(request):
    try:
        city = request.slots.get('City')
        state = request.slots.get('State')
        (lat,lon) = city_to_lat_long(city, state)

        aqi = get_air_quality_index(lat, lon)
        return alexa.create_response(message=get_response_by_aqi(aqi, city), end_session=True)

    except Exception:
        return alexa.create_response(message=RESPONSES['not_found'], end_session=True)

def get_zip(apiAccessToken, deviceId, host):
    url = '{}/v1/devices/{}/settings/address/countryAndPostalCode'.format(
        host,
        deviceId
    )
    headers = {
        "Content-Type": "application/json",
        "Authorization": "Bearer {}".format(apiAccessToken)
    }

    try:
        resp = requests.get(url, headers=headers)
        zip_code = resp.json().get('postalCode')
        print(resp.json())
        return zip_code
    except:
        raise ApiException('Could not get zip code from Amazon')

def zip_to_lat_long(zip_code):
    # https://gist.github.com/erichurst/7882666
    with open('zip_codes.csv', 'r') as fh:
        for line in fh.readlines():
            if line.split(',')[0] == zip_code:
                return line.replace('\n', '').split(',')[1:3]

        raise Exception('Could not find lat/lon match')

def city_to_lat_long(city, state=None):
    with open('worldcities.csv', 'r') as fh:
        for line in fh.readlines():
            if line.split(',')[0].lower() == city.lower():
                if not state or state.lower() == line.replace('\n', '').replace('\r', '').split(',')[4].lower():
                    return line.replace('\n', '').replace('\r', '').split(',')[1:3]

def get_air_quality_index(lat, lon):
    retries = 0
    done = False

    while not done and retries < 3:
        try:
            url = 'https://api.waqi.info/feed/geo:{0:.1f};{1:.1f}/?token={2}'.format(
                float(lat),
                float(lon),
                AQICN_API_KEY
            )

            resp = requests.get(url)
            aqi = resp.json().get('data', {}).get('aqi', -1)
            done = True
        except Exception:
            retries += 1
            aqi = -1

    return aqi

# TODO - Put location back into responses
def get_response_by_aqi(aqi, city=None):
    resp_template = RESPONSES['air_quality']
    response = 'unknown'

    loc = ' outside.'
    if city:
        loc = ' in ' + city + '.'

    # TODO: better responses: http://aqicn.org/scale/
    if aqi >= 0 and aqi <= 50:
        response = 'good'
    elif aqi > 50 and aqi <= 100:
        response = 'moderate. Sensitive people should limit prolonged outdoor exertion'
    elif aqi > 100 and aqi <= 150:
        response = 'unhealthy for sensitive groups. Sensitive people should limit prolonged outdoor exertion'
    elif aqi > 150 and aqi <= 200:
        response = 'unhealthy. Sensitive people should avoid prolonged outdoor exertion. Everyone welse should limit prolonged outdoor exertion'
    elif aqi > 200 and aqi <= 300:
        response = 'very unhealthy. Sensitive people should avoid all outdoor exertion. Everyone else should limit outdoor exertion.'
    elif aqi > 300:
        response = 'hazardous. Everyone should avoid all outdoor exertion'

    return resp_template.format(response)
